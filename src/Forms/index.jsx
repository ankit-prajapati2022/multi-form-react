import React from "react";
import "./styles.css";

const Index = () => {
  const [formData, setFormData] = React.useState({});

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData);
  };

  return (
    <div className="container-fluid" id="grad1">
      <div className="row justify-content-center mt-0">
        <div className="col-11 col-sm-9 col-md-7 col-lg-6 text-center p-0 mt-3 mb-2">
          <div className="card px-0 pt-4 pb-0 mt-3 mb-3">
            <h2>
              <strong>Form</strong>
            </h2>
            <p>Acc</p>
            <div className="row">
              <div className="col-md-12 mx-0">
                <form id="msform">
                  <ul id="progressbar">
                    <li className="active" id="account">
                      <strong>Step 1</strong>
                    </li>
                    <li id="personal">
                      <strong>Step 2</strong>
                    </li>
                    <li id="payment">
                      <strong>Step 3</strong>
                    </li>
                    <li id="confirm">
                      <strong>Finish</strong>
                    </li>
                  </ul>
                  <fieldset>
                    <div className="form-card">
                      <h2 className="fs-title text-center mb-2">
                        Welcome ! Lets Start !!!!
                      </h2>
                      <h3 className="fs-title text-center font-weight-light mb-5">
                        Fill out your information below
                      </h3>
                      <div className="input-effect">
                        <input
                          className="effect-20"
                          type="text"
                          name="name"
                          placeholder="Name"
                          onChange={handleChange}
                        />
                        <label>Name</label>
                        <span className="focus-border">
                          <i></i>
                        </span>
                      </div>
                      <div className="input-effect">
                        <input
                          className="effect-20"
                          type="text"
                          name="Dname"
                          placeholder="Display Name"
                          onChange={handleChange}
                        />
                        <label>Display Name</label>
                        <span className="focus-border">
                          <i></i>
                        </span>
                      </div>
                    </div>
                    <input
                      type="button"
                      name="next"
                      className="next action-button"
                      value="Next Step"
                    />
                  </fieldset>
                  <fieldset>
                    <div className="form-card">
                      <h2 className="fs-title text-center mb-2">
                        Lets setup the workspace !!!
                      </h2>
                      <h3 className="fs-title text-center font-weight-light mb-5">
                        Create a workspace
                      </h3>
                      <div className="input-effect">
                        <input
                          className="effect-20"
                          type="text"
                          name="wname"
                          placeholder="Workspace Name"
                          onChange={handleChange}
                        />
                        <label>Workspace Name</label>
                        <span className="focus-border">
                          <i></i>
                        </span>
                      </div>
                      <div className="input-effect">
                        <input
                          className="effect-20"
                          type="text"
                          name="wurl"
                          placeholder="Workspace URL"
                          onChange={handleChange}
                        />
                        <label>Workspace URL</label>
                        <span className="focus-border">
                          <i></i>
                        </span>
                      </div>
                    </div>
                    <input
                      type="button"
                      name="next"
                      className="next action-button"
                      value="Next Step"
                    />
                  </fieldset>
                  <fieldset>
                    <div className="form-card">
                      <h2 className="fs-title text-center mb-2">
                        How are you planning to use wokspace?
                      </h2>
                      <h3 className="fs-title text-center font-weight-light mb-5">
                        We'll stream line your setup accordingly.
                      </h3>
                      <div className="radio-text">
                        <input
                          type="radio"
                          name="workspaceuse"
                          id="radio-button-1"
                          value="Myslef"
                          onChange={handleChange}
                        />
                        <input
                          type="radio"
                          name="workspaceuse"
                          id="radio-button-2"
                          value="My Team"
                          onChange={handleChange}
                        />
                        <label
                          htmlFor="radio-button-1"
                          className="radio-button-1"
                        >
                          <h5>For Myself</h5>
                          <p>
                            Write better.Think more clearly. Stay organized.
                          </p>
                        </label>
                        <label
                          htmlFor="radio-button-2"
                          className="radio-button-2"
                        >
                          <h5>With my team</h5>
                          <p>Wikis,docs,tasks and projects all in one space.</p>
                        </label>
                      </div>
                    </div>
                    <input
                      type="button"
                      name="make_payment"
                      className="next action-button"
                      value="Confirm"
                      onClick={handleSubmit}
                    />
                  </fieldset>
                  <fieldset>
                    <div className="form-card">
                      <div className="row justify-content-center">
                        <div className="col-3">
                          <img
                            src="https://img.icons8.com/color/96/000000/ok--v2.png"
                            className="fit-image"
                            alt="success"
                          />
                        </div>
                      </div>
                      <br />
                      <h2 className="fs-title text-center">Success !</h2>
                      <div className="row justify-content-center">
                        <div className="col-7 text-center">
                          <h5>You Have Successfully Signed Up</h5>
                        </div>
                      </div>
                    </div>
                    <input
                      type="button"
                      name="make_payment"
                      className="next action-button"
                      value="Launch Application"
                    />
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Index;
